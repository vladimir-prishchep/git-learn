# Training-Management-System
> Application for managing educational trainings and courses

## Build Requirements

##### Common
  * `maven` (`v3.1.0` or higher)
  * `jdk` (`v1.8` or higher)
  * `node.js` (`v0.12.7` or higher)

## Build Commands
##### Run to develop
1) Set flag in file ``frontend\src\app\services\API\url.service.js``


    var isApi = true;


2) Set flag in file ``src\main\resources\application.properties``


    spring.profiles.active = develop


3) All frontend commands run in ``\frontend`` folder, type in command line:


    cd frontend


4) Install all frontend dependencies:


    npm install
    bower install


5) Run all frontend tasks and start dynamically watch development changes:


    gulp serve


6) Start backend (initialise connection to DB):


    mvn spring-boot:run


##### Run to build
1) Set flag in file ``frontend\src\app\services\API\url.service.js``


    var isApi = false;


2) Set flag in file ``src\main\resources\application.properties``


        spring.profiles.active = build


3) To generate war:


        mvn clean install


4) To run war:


        java -jar training-1.0-SNAPSHOT.war


##### Run with LDAP
Add flag in file ``src\main\resources\application.properties``

    spring.profiles.active = **,ldap

##### Run without LDAP
Add flag in file ``src\main\resources\application.properties``

    spring.profiles.active = **,dbAuth